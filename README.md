# PDF Toolkit - Deployment

*Note: This project had been migrated from a private repository and hence does not include the pre-migration pipeline history.*

## Overview

This repository contains all the deployment files necessary to set up the Kubernetes cluster and provides an explanation of the overall setup sequence.

> For an explanation on the DevOps portion (i.e. K8s components, CI/CD config), refer to the
> [deployment documentation Markdown file](deployment.md).

Docker images utilised are stored in GCP Container Registry:
- `asia.gcr.io/pdftoolkit/tkbe`
- `asia.gcr.io/pdftoolkit/tkfe`
- `asia.gcr.io/pdftoolkit/tika`

For the `tkfe`, `tkbe` and `tika` containers, there are specified configurations for liveness and readiness probes:

- Liveness probes are used to determine when to restart a container.
- Readiness probes are used to know when a container is ready to start accepting traffic. A pod is considered ready when all of its containers are ready.

The `/scripts` folder contains shell scripts that will set up the entire Kubernetes cluster. The scripts should be run in the order as shown in the following table.

| Filename | Purpose |
| --- | --- |
| `create-k8s-cluster.sh` | Creates a Kubernetes cluster `pdftk` |
| `load-deployment-files.sh` | *(optional)* Load YAML deployment files from the `pdftk-k8s-deployment` GCS bucket |
| `setup-k8s-cluster.sh` | Creates a custom namespace and installs Helm |
| `create-service-account.sh` | *(optional)* Creates a service account with admin access to GCS |
| `reserve-static-ip.sh` | *(to be done before creating the ingress)* Reserves a static IP address for the LoadBalancer |
| `install-in-k8s-cluster.sh` | Installs `elasticsearch`, `tkbe`, `tika`, `tkfe` and configures the Nginx Ingress controller to expose the `tkfe` and `tkbe` services |

## Creating the Kubernetes cluster

**Reference script: `create-k8s-cluster.sh`**

Creation of the Kubernetes cluster is handled in the following command:

```bash
$ gcloud container clusters create ${CLUSTER_NAME} \
    --zone ${INSTANCE_ZONE} \
    --cluster-version 1.14.8-gke.12 \
    --num-nodes 2 \
    --enable-autoscaling --min-nodes 1 --max-nodes 3 \
    --enable-autoupgrade \
    --enable-autorepair
```

It sets the following parameters:

- Default to 2 nodes
- Autoscaling is enabled for Kubernetes to assign 1-3 nodes depending on demand
- Autoupgrade will update the nodes with the latest version of Kubernetes
- Autorepair helps keep the nodes healthy

The following command sets up `kubectl` with the newly created cluster.

```bash
$ gcloud container clusters --zone <zone> get-credentials <cluster name>
```

## Setting up the Kubernetes cluster

**Reference script: `setup-k8s-cluster.sh`**

### Creating a custom namespace

Creates a custom namespace titled `production`. Using namespaces helps to segregate between different environments.

```bash
$ kubernetes apply -f namespace.yml
# switch to the newly created namespace
$ kubectl config set-context --current --namespace=production
```

### Installing Helm

Helm is a package manager for Kubernetes, similar to NPM for Node.js.

Helm is the client side; Tiller is the server-side of Helm that installs Helm charts in the Kubernetes cluster.

![Helm-Tiller diagram](helm/helm-tiller.png)

The script contains the following steps for Helm installation:

1. Apply `helm/helm-service-account.yml`. This does 2 things:
    1. Adds a service account within a `kube-system` namespace to segregate Tiller
    1. Creates a cluster role binding for Tiller as the cluster admin
1. Initialise Helm within the Tiller service account
    ```bash
    $ helm init --service-account tiller
    ```
1. Updates repositories for Helm repository integration
    ```bash
    $ helm repo update
    ```

Reference:

- [Installing Helm in GKE](https://medium.com/google-cloud/installing-helm-in-google-kubernetes-engine-7f07f43c536e)

## Elasticsearch

**Reference script: `install-in-k8s-cluster.sh`**

Install Elasticsearch using a Helm chart.

```bash
$ helm install --name elasticsearch elastic/elasticsearch \
    --set replicas=1 \
    --set resources.requests.memory=1Gi \
    --set volumeClaimTemplate.resources.requests.storage=100Gi \
    --set readinessProbe.initialDelaySeconds=60 \
    --set imageTag=7.4.1 \
    --set esMajorVersion=7
```

The specified parameters will:

- Create only 1 Elasticsearch pod (default 3)
- Set 1GB RAM for the stateful set
- Set a 100GB storage capacity limit on the stateful set
- Set a delay of 60 seconds before launching a readiness probe

Reference:

- [Elasticsearch Helm Chart - Configuration](https://github.com/elastic/helm-charts/tree/master/elasticsearch#configuration)

## PDF Toolkit - Backend

**Reference script: `install-in-k8s-cluster.sh`**

The backend connects with 3 external sources:

1. Elasticsearch pod
    - Running within the same cluster so accessible via internal cluster IP
1. Tika server
    - Running within the same pod so accessible via `localhost`
1. GCS bucket
    - Requires read/write access to the `pdftk-k8s-storage` bucket to access the `changelog.json` and `feedback.json` files
    - A secret key of the service account with the requisite permissions is required

### Creating a secret for the GCP service account

First, create a GCP service account with "Storage Admin" role and download the JSON key:

```bash
# create a service account
$ gcloud iam service-accounts create <SA name> \
    --description <SA description> \
    --display-name <SA display name>
# add "Storage Admin" role to this account
$ gcloud projects add-iam-policy-binding <project name> \
    --member=serviceAccount:<service account email>
    --role=roles/storage.admin
# download service account JSON key
$ gcloud iam service-accounts keys create <output filename> --iam-account=<service account email>
```

The generic command to create a Kubernetes secret is:<br/>
`kubectl create secret <type> <name of secret> --from-file=<name of secret key>=<filepath>`

In this instance, run the command (replace `gcs-admin-key.json` with the filename of the service account JSON key downloaded above):

```bash
$ kubectl create secret generic gcs-admin-key \
    --from-file=gcs-admin-key=gcs-admin-key.json
```

### Apply YAML files

Apply the following YAML files:

1. `tkbe/tkbe-config.yml`
    - Sets config map for the `ELASTICSEARCH_IP` and `ELASTICSEARCH_PORT` variables
1. `tkbe/tkbe-deployment.yml`
    - Creates a `tkbe` pod running 2 containers:
        - `tkbe`
            - Runs the Python backend on a GUnicorn WSGI server at port 5005 
            - Mounts the `gcs-admin-key` data volume in the `/app/misc` folder to allow access from within the Python code for read/write access to the `pdftk-k8s-storage` bucket
            - Connects to the Elasticsearch pod at the internal cluster IP and port as specified in `tkbe-config.yml`
        - `tika`
            - Runs an Apache Tika server at port 9998
    - Mounts the `gcs-admin-key` secret as a data volume in the Kubernetes cluster
1. `tkbe/tkbe-service.yml`
    - Exposes the `tkbe` pod as a service with an internal cluster IP
    - Access to this service is covered by the [Nginx Ingress controller](#nginx-ingress-controller)

## PDF Toolkit - Frontend

**Reference script: `install-in-k8s-cluster.sh`**

Apply the following YAML files:

1. `tkfe/tkfe-deployment.yml`
    - Creates a `tkfe` pod running a `tkfe` container that serves the static frontend site on port 80
1. `tkfe/tkfe-service.yml`
    - Exposes the `tkfe` pod as a service with an internal cluster IP
    - Access to this service is covered by the [Nginx Ingress controller](#nginx-ingress-controller)

## Nginx Ingress Controller

**Reference script: `install-in-k8s-cluster.sh`**

### Reserving a static IP address

If not already reserved, reserve a static IP address that will be attached to the load balancer.

```bash
$ gcloud compute addresses create pdftk \
  --region asia-southeast1
# view reserved IP address
$ gcloud compute addresses list
```

### Installing the Nginx Ingress Controller

First, install the Nginx Ingress controller using a Helm chart. Replace *<reserved IP>* with the static IP address that you have reserved; this attaches the IP address to the Nginx load balancer service.

```bash
$ helm install --name nginx-ingress stable/nginx-ingress \
    --set controller.service.loadBalancerIP=<reserved IP> \
    --set rbac.create=true \
    --set controller.publishService.enabled=true
```

Then, apply the `ingress/ingress.yml` file to create an Ingress that exposes 2 paths:

- `/`
    - Maps to `tkfe` service at port 80
- `/api`
    - Maps to `tkbe` service at port 5005


References:

- [Ingress with NGINX controller on Google Kubernetes Engine](https://cloud.google.com/community/tutorials/nginx-ingress-gke)
- [Configuring Domain Names with Static IP Addresses](https://cloud.google.com/kubernetes-engine/docs/tutorials/configuring-domain-name-static-ip)
