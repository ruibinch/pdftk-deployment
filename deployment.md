# PDF Toolkit - Deployment Details

## Overview

The app is deployed in **Docker containers** on **Google Kubernetes Engine** with **GitLab CI/CD** used as the DevOps service.

## Kubernetes cluster setup

The Kubernetes cluster comprises of the following components:

- 1 load balancer
- 1 ingress
- 2 services
    - Frontend
    - Backend
- 3 pods
    - Frontend
    - Backend
    - Elasticsearch

![tkdeploy_cluster](docs_tkdeploy_cluster.png)

### Load Balancer/Ingress

**Nginx Ingress controller** is used to manage the web server and load balancer portion of the app. This is installed using a Helm chart in the K8s cluster.

The load balancer IP is set to **`35.198.253.11`**, a static IP address that had been pre-reserved in GCP.

The ingress resource exposes 2 endpoints:

1. `/`
    - Maps to `tkfe` service at port 80
1. `/api`
    - Maps to `tkbe` service at port 5005

### Services

Services are used to expose the deployments (and underlying pods) that they are associated to. Services are necessary as a middleman between the incoming traffic and the deployment, as the IP address of the pods in the deployments are not static.

There are 2 services:

1. `tkfe`
    - Forwards traffic on to the `tkfe` pod
1. `tkbe`
    - Forwards traffic on to the `tkbe` pod

### Pods

Pods are the smallest unit in a K8s cluster. They are typically configured via a *Deployment* object instead of being directly managed.

Pods run 1 or more *containers* within them; multiple ports can be exposed on a pod depending on the containers within. 

There are 3 pods:

1. `tkfe`
    - Runs a `tkfe` container that serves the **static frontend site on port 80**
1. `tkbe`
    - Runs a `tkbe` container that serves the **Python backend on a GUnicorn WSGI server at port 5005**
    - Runs a `tika` container that launches an **Apache Tika server at port 9998**
1. `elasticsearch`
    - Runs an **Elasticsearch server at port 9200**; installed via a Helm chart

The `tkbe` pod connects to the `elasticsearch` service via an internal cluster IP. As the service is not exposed via the ingress resource, it is not accessible from outside of the cluster.

## DevOps

The `Dockerfile` lists the commands to build a Docker image of the apps.

The CI/CD pipelines are configured via a `.gitlab-ci.yml` file.

### Frontend

#### Dockerfile

```
FROM node:13-alpine AS builder
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn
COPY . .
RUN yarn build

FROM node:13-alpine
RUN yarn global add serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "80", "-s", "."]
```

A multi-stage build is used in this Dockerfile configuration:

1. Build
    - Install Node.js package dependencies and build project
1. Serve
    - Copies the build files from the first stage and serves the app

This helps to keep the final built Docker image lean as the dependencies, most notably the `node_modules` folder, are excluded.

#### CI/CD

```
image: node:13.1-alpine

stages:
  - test
  - dockerise
  - deploy

cache:
  paths:
    # cache this dependency folder between builds
    - node_modules/

testing:
  stage: test
  when: manual
  allow_failure: false
  script:
    - yarn
    - yarn test

dockerise_app:
  stage: dockerise
  image: docker:19.03
  services:
    - docker:19.03-dind
  variables:
    DOCKER_IMAGE_TAG: asia.gcr.io/pdftoolkit/tkfe
    # Ref: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-disabled
    # when using dind service, we need to instruct docker to connect with the daemon
    # started inside of the service. The daemon is available with a network connection
    # instead of the default /var/run/docker.sock socket.
    DOCKER_HOST: tcp://docker:2375
    # instruct Docker not to start over TLS
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker info
    # login to Docker using GCS admin service account key to allow push to GCR
    - cat $GCS_ADMIN_SA_KEY | docker login -u _json_key --password-stdin https://asia.gcr.io
  script:
    # Ref: https://andrewlock.net/caching-docker-layers-on-serverless-build-hosts-with-multi-stage-builds---target,-and---cache-from/
    # pull the latest builder image
    - docker pull ${DOCKER_IMAGE_TAG}:builder || true
    # build only the builder stage
    - docker build --target builder --cache-from ${DOCKER_IMAGE_TAG}:builder -t ${DOCKER_IMAGE_TAG}:builder .
    # build whole Dockerfile, using the pulled builder image as a cache
    - docker build --cache-from ${DOCKER_IMAGE_TAG}:builder -t ${DOCKER_IMAGE_TAG}:alpha .

    # push builder image to be used for the next build
    - docker push ${DOCKER_IMAGE_TAG}:builder
    # push runtime image
    - docker push ${DOCKER_IMAGE_TAG}:alpha

update_k8s_deployment:
  stage: deploy
  image: google/cloud-sdk:alpine
  variables:
    CLUSTER_NAME: pdftk
    INSTANCE_ZONE: asia-southeast1-a
    DEPLOYMENT_NAME: tkfe
    K8S_NAMESPACE: production
  before_script:
    - gcloud components install kubectl
  script:
    - gcloud auth activate-service-account --key-file $K8S_DEPLOYER_SA_KEY
    # set config and associate kubectl to the cluster
    - gcloud config set container/cluster ${CLUSTER_NAME}
    - gcloud config set compute/zone ${INSTANCE_ZONE}
    - gcloud container clusters get-credentials ${CLUSTER_NAME}
    # update deployment
    - kubectl config set-context --current --namespace="$K8S_NAMESPACE"
    - kubectl patch deployment ${DEPLOYMENT_NAME} -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
```

There are 3 stages in the CI/CD pipeline:

1. `test`
    - Runs unit tests using Jest and Enzyme
1. `dockerise`
    - Uses a Docker-in-Docker service to connect to a Docker daemon running inside the Docker container that is running the CI/CD
    - Logs in to Docker using a GCP service account key with "Storage Admin" privileges
    - Pulls the latest Docker image with `builder` tag and uses this as a cache from which to build the new image
        - This helps to shave off a significant amount of time in the CI/CD pipeline as the app does not need to be re-built everytime; it is only necessary when the dependencies have been modified
    - Builds 2 new docker images:
        1. `builder` tag - Only builds the `build` stage specified in the `Dockerfile`; this will then be used as a cache from which subsequent images can be built on
        1. Deployment tag - To be used as a reference from the K8s pod
    - Pushes the 2 docker images to Google Container Registry
1. `deploy`
    - Configures the `gcloud` credentials and installs `kubectl`
    - Patches the *deployment* object by setting the `annotations` label in the metadata to the current date - this will force the pod to update and thus to pull the latest Docker image from the registry

### Backend

#### Dockerfile

```
FROM python:3.7-alpine

WORKDIR /app

RUN pip install virtualenv && \
    virtualenv -p python3.7 /venv
ENV VIRTUAL_ENV /venv
ENV PATH /venv/bin:$PATH

COPY requirements.txt .
RUN pip install -r requirements.txt && \
    mkdir logs

COPY . .

EXPOSE 5005

CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:5005", "wsgi:app"]
```

The Dockerfile configuration consists of the following steps:

1. Create a virtual environment
1. Install necessary Python packages
1. Copies the codebase over into the Docker image
1. Exposes the app via a GUnicorn WSGI HTTP server on port 5005

#### CI/CD

```
image: python:3.7-alpine

stages:
  - test
  - dockerise
  - deploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - venv/

unit_test:
  stage: test
  when: manual
  allow_failure: false
  script:
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install -r requirements.txt
    - mkdir logs
    - pytest

dockerise_app:
  stage: dockerise
  image: docker:19.03
  services:
    - docker:19.03-dind
  variables:
    DOCKER_IMAGE_TAG: asia.gcr.io/pdftoolkit/tkbe
    # Ref: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-disabled
    # when using dind service, we need to instruct docker to connect with the daemon
    # started inside of the service. The daemon is available with a network connection
    # instead of the default /var/run/docker.sock socket.
    DOCKER_HOST: tcp://docker:2375
    # instruct Docker not to start over TLS
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker info
    # authenticate to GCP Container Registry using service account key
    - cat $GCS_ADMIN_SA_KEY | docker login -u _json_key --password-stdin https://asia.gcr.io
  script:
    - docker pull ${DOCKER_IMAGE_TAG}:alpha || true
    - docker build --cache-from ${DOCKER_IMAGE_TAG}:alpha -t ${DOCKER_IMAGE_TAG}:alpha .
    # -t "${DOCKER_IMAGE_TAG}:${CI_COMMIT_SHA:0:8}"
    - docker push ${DOCKER_IMAGE_TAG}:alpha
    # - docker push ${DOCKER_IMAGE_TAG}:${CI_COMMIT_SHA:0:8}
    
update_k8s_deployment:
  stage: deploy
  image: google/cloud-sdk:alpine
  variables:
    CLUSTER_NAME: pdftk
    INSTANCE_ZONE: asia-southeast1-a
    DEPLOYMENT_NAME: tkbe
    K8S_NAMESPACE: production
  before_script:
    - gcloud components install kubectl
  script:
    - gcloud auth activate-service-account --key-file $K8S_DEPLOYER_SA_KEY
    # set config and associate kubectl to the cluster
    - gcloud config set container/cluster ${CLUSTER_NAME}
    - gcloud config set compute/zone ${INSTANCE_ZONE}
    - gcloud container clusters get-credentials ${CLUSTER_NAME}
    # update deployment
    - kubectl config set-context --current --namespace="$K8S_NAMESPACE"
    - kubectl patch deployment ${DEPLOYMENT_NAME} -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
```

There are 3 stages in the CI/CD pipeline:

1. `test`
    - Sets up a virtual Python environment
    - Runs unit tests using `pytest` 
1. `dockerise`
    - Uses a Docker-in-Docker service to connect to a Docker daemon running inside the Docker container that is running the CI/CD
    - Logs in to Docker using a GCP service account key with "Storage Admin" privileges
    - Pulls the latest Docker image and uses this as a cache from which to build the new image
    - Builds a new image, using the previous latest image as a cache
    - Pushes the new docker image to Google Container Registry
1. `deploy`
    - Configures the `gcloud` credentials and installs `kubectl`
    - Patches the *deployment* object by setting the `annotations` label in the metadata to the current date - this will force the pod to update and thus to pull the latest Docker image from the registry
