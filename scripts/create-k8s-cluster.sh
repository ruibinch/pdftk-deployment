#!/usr/bin/env bash

echo "ECHO: set cluster config"
export GCLOUD_PROJECT=$(gcloud config get-value project)
export INSTANCE_ZONE=asia-southeast1-a
export CLUSTER_NAME=pdftk
gcloud config set project ${GCLOUD_PROJECT}

echo "ECHO: create cluster"
gcloud container clusters create ${CLUSTER_NAME} \
  --zone ${INSTANCE_ZONE} \
  --disk-type pd-standard --disk-size 100 \
  --preemptible
  --num-nodes 2 \
  --enable-autoscaling --min-nodes 2 --max-nodes 4 \
  --enable-autoupgrade \
  --enable-autorepair

echo "ECHO: confirm cluster is running"
gcloud container clusters list

echo "ECHO: setup kubectl with the cluster"
gcloud container clusters get-credentials ${CLUSTER_NAME} \
  --zone ${INSTANCE_ZONE}

echo "ECHO: confirm connection to cluster"
kubectl cluster-info
