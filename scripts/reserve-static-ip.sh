#!/usr/bin/env bash

echo "ECHO: reserving a static IP address in region \"asia-southeast1\""
gcloud compute addresses create pdftk \
  --region asia-southeast1
gcloud compute addresses list
