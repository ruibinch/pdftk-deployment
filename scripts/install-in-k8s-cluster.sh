#!/usr/bin/env bash

echo "ECHO: installing tkbe pod"
kubectl apply -f tkbe/tkbe-deployment.yml
echo "ECHO: installing tkbe service"
kubectl apply -f tkbe/tkbe-service.yml
