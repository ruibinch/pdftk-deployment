#!/usr/bin/env bash

echo "ECHO: creating a namespace titled \"production\""
kubectl apply -f namespace.yml
echo "ECHO: switch to this \"production\" namespace"
kubectl config set-context --current --namespace=production
kubectl config view | grep namespace

echo "ECHO: install helm"
kubectl apply -f helm/helm-service-account.yml

echo "ECHO: initialise helm"
helm init --service-account tiller
helm repo update

echo "ECHO: verify helm installation"
kubectl get deploy,svc tiller-deploy -n kube-system
