#!/usr/bin/env bash

echo "ECHO: load k8s deployment files from pdftk-deployment GCS bucket"
cd ~
mkdir pdftoolkit
cd pdftoolkit
gsutil rsync -r gs://pdfparserr-deployment .
